package client

import (
	"google.golang.org/grpc"
	"todo_service/config"
	"todo_service/protos"
)

type IServiceManager interface {
	TodoService() todo_service.TodoServiceClient
}

type grpcClients struct {
	todoService todo_service.TodoServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManager, error) {
	connTodoService, err := grpc.Dial(
		cfg.GRPCHost+cfg.GRPCPort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		todoService: todo_service.NewTodoServiceClient(connTodoService),
	}, nil
}

func (g *grpcClients) TodoService() todo_service.TodoServiceClient {
	return g.todoService
}
