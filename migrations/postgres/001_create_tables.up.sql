create table todos (
    id uuid primary key not null,
    name varchar(30) not null,
    deadline timestamp,
    status bool
);
